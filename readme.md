# Fidelis - Data-validation utility

This utility used to identify if any data mismatches are present in the SQL (on-premise) database and solr database.
  

# How to use this utility:

  - Clone this repo to local machine.
  - In IntelliJ IDEA IDE , import this project as a gradle project.
  - Navigate to the edit configuration section and set the two command line arguments ( *application.properties and input.json file paths* )


# How to generate this utility as zip / tar file

 - Open the terminal in cloned directory.
 - Run the below commands.
 ```
 ./gradlew clean
 ./gradlew build
 ```
Note: zip and tar file are present in the below location. 
    */build/distributions/

# How to get the Mismatched data

- Configure the Onpremise database details and solr details in the conf folder.
- Navigate to the bin directory and execute the below command
```
./dataValidationUtility ../conf/application.properties ../conf/input.json
```
- check the console for mismatched messages list.
### Sample input.json file

```
[
  {
    "onPremise": {
      "select": [
        "sql_col1",
        "sql_col2",
        "sql_col3",
        "sql_col4"
      ],
      "from": "table_name",
      "where": "where condition if any",
      "orderBy": "orderby column Name"
    },
    "solr": {
      "collection": "Collection_name",
      "fl": [
        "solr_col1",
        "solr_col2",
        "solr_col3",
        "solr_col4"
      ],
      "sortOrder": "asc or desc",
      "sortField": "sortfield like solr_col4"
    },
    "mappings": {
      "sql_col1": "solr_col1",
      "sql_col2": "solr_col2",
      "sql_col3": "solr_col3",
      "sql_col4": "solr_col4"
    }
  },
  {
    .........
  },
  {
    .........
    }
]
```
