package com.talentica.fidelis.datavalidation;

import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;
import org.json.JSONArray;

import java.util.Date;

public class sendEmail{

    public static void sendmail(String fileName, JSONArray CC, String TO)
    {
        EmailAttachment emailAttachment=new EmailAttachment();
        emailAttachment.setPath(fileName);
        emailAttachment.setDisposition(EmailAttachment.ATTACHMENT);
        emailAttachment.setDescription("Picture Attachment");
        emailAttachment.setName(fileName);
        MultiPartEmail email=new MultiPartEmail();

        try {
//            email.setHostName("smtp.gmail.com");
            email.setFrom("no-reply@fidelis-inc.com");
            email.setSubject("Data Validation - " + (new Date()));
            email.setMsg("This is Auto generated mail.");
            email.addTo(TO);
            for(int i=0;i<CC.length();i++) {
                email.addCc(CC.getString(i));
            }
            email.attach(emailAttachment);
            email.send();
        } catch (EmailException e) {
            e.printStackTrace();
        }
    }

}
