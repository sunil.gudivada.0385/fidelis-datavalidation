package com.talentica.fidelis.datavalidation;

import freemarker.template.DefaultObjectWrapper;
import freemarker.template.TemplateException;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public class onPremise {
    private static final Logger LOG = LoggerFactory.getLogger(onPremise.class);

    private static File getFileFromResources(String fileName) {
        ClassLoader classLoader = onPremise.class.getClassLoader();
        URL resource = classLoader.getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("file is not found!");
        } else {
            return new File(resource.getFile());
        }
    }

    public String generateSQLQuery(JSONObject queryParams) throws IOException, TemplateException {
        LOG.debug("Inside the SQL Query genarator");
        Map<String, Object> input = new HashMap<String, Object>();

        input.put("select",queryParams.get("select"));
        input.put("from",queryParams.getString("from"));
        input.put("where",queryParams.getString("where"));
        input.put("orderBy",queryParams.getString("orderBy"));

        String template = "select <#list 0..select.length()-1 as i> ${select.get(i)} <#sep>, </#list> from ${from} <#if where?has_content>where ${where} </#if> <#if orderBy?has_content>order by ${orderBy} ASC</#if>";

        Configuration cfg = new Configuration();
        cfg.setObjectWrapper(new DefaultObjectWrapper());
        Template t = new Template("queryGenerationTemplate", new StringReader(template), cfg);
        Writer out = new StringWriter();
        t.process(input, out);
        String transformedTemplate = out.toString();
        LOG.debug("Query:::" + transformedTemplate);
        return transformedTemplate;
    }

    public JSONArray getData(JSONObject params, JSONObject queryParams){
        LOG.debug(":::Inside Get Data");
        LOG.debug(":::query params:::"+queryParams);
        JSONArray responseData = new JSONArray();
        String url = params.getString("jdbcurl")+"?characterEncoding=latin1";
        String user = params.getString("user");
        String pass = params.getString("pass");
        Connection con = null;
        String sql = null;
        try
        {
            JSONArray cols = queryParams.getJSONArray("select");
            sql = generateSQLQuery(queryParams);
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            con = DriverManager.getConnection(url,user,pass);
            Statement st = con.createStatement();
            LOG.debug(":::Before Query Execution");
            ResultSet rs = st.executeQuery(sql);
            LOG.debug(":::After Query Execution");
            while(rs.next()) {
                JSONObject tempTableData = new JSONObject();
                for(int i=0;i<cols.length();i++){
                    tempTableData.put(cols.getString(i),rs.getObject(i+1));
                }
                responseData.put(tempTableData);
            }
            con.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        } catch(Exception ex) {
            System.err.println(ex);
        }
        LOG.debug(":::Sending response Data"+responseData);
        return responseData;
    }

}
