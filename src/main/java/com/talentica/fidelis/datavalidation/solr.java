package com.talentica.fidelis.datavalidation;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;

public class solr {
    private static final Logger LOG = LoggerFactory.getLogger(solr.class);

    public JSONArray getData(JSONObject params,JSONObject queryParams,JSONObject mappings) throws IOException, SolrServerException {
        LOG.debug(":::Inside Get Data");
        LOG.debug(":::query params:::"+queryParams);
        JSONArray responseData = new JSONArray();
        String urlString = params.getString("solrurl")+"/"+queryParams.getString("collection");
        JSONArray cols = (JSONArray) queryParams.get("fl");

        LOG.debug(":::url String:::"+urlString);
        SolrClient Solr = new HttpSolrClient.Builder(urlString).build();

        SolrQuery query = new SolrQuery();
        query.setQuery("*:*");
        if(queryParams.getString("sortOrder").equals("asc")) {
            query.setSort(queryParams.getString("sortField"), SolrQuery.ORDER.asc);
        }else {
            query.setSort(queryParams.getString("sortField"), SolrQuery.ORDER.desc);
        }
        for(int i=0;i<cols.length();i++){
            query.addFacetField(cols.getString(i));
        }

        QueryResponse queryResponse = Solr.query(query);

        LOG.debug(":::getting query results");
        SolrDocumentList docs = queryResponse.getResults();

        for(int i=0;i<docs.size();i++){
            JSONObject tempDocData = new JSONObject();
            for(int j=0;j<cols.length();j++){
                ArrayList tempArray = (ArrayList) docs.get(i).get(cols.get(j));
                JSONObject put = tempDocData.put(mappings.getString(cols.getString(j)), tempArray.get(0).toString());
            }
            responseData.put(tempDocData);
        }
        Solr.commit();
        LOG.debug(":::Sending response Data"+responseData);
        return responseData;
    }

}
