package com.talentica.fidelis.datavalidation;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class validator {

    private static final Logger LOG = LoggerFactory.getLogger(validator.class);
    static ExtentTest test;
    static ExtentReports report;

    public static String readMetadataFiles(String filePath) throws Exception {
        LOG.debug("::: Reading JSON Content");
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(filePath));

            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }

            return sb.toString();
        } catch (Exception e) {
            throw e;
        } finally {
            if (br != null) {
                br.close();
            }
        }
    }

    public static Properties readPropertiesFile(String fileName) throws IOException {
        FileInputStream fis = null;
        Properties prop = null;
        try {
            fis = new FileInputStream(fileName);
            prop = new Properties();
            prop.load(fis);
        } catch(FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        } catch(IOException ioe) {
            ioe.printStackTrace();
        } finally {
            fis.close();
        }
        return prop;
    }

    public static void main(String[] args) throws Exception {
        LOG.debug(":::Inside Main");
        JSONObject SQLparams = new JSONObject();
        JSONObject solrParams = new JSONObject();
        JSONObject sqlToSolrColMapping = new JSONObject();

        JSONArray SQLColList = new JSONArray();
        Properties prop = readPropertiesFile(args[0]);

        JSONObject paramDetails = new JSONObject();
        paramDetails.put("user",prop.getProperty("sql.user"));
        paramDetails.put("pass",prop.getProperty("sql.pass"));
        paramDetails.put("jdbcurl",prop.getProperty("sql.jdbc_url"));
        paramDetails.put("solrurl",prop.getProperty("solr.base_url"));

        JSONArray jsonArray = new JSONArray(readMetadataFiles(args[1]));
        JSONArray resultMessages = new JSONArray();

        DateFormat formatter = new SimpleDateFormat("dd_MM_yyyy");

        long today = new Date().getTime();
        String fileName = System.getProperty("user.dir")+"/../reports/Report-"+today+".html";
        report = new ExtentReports(fileName);

        for(int a=0;a<jsonArray.length();a++){

            LOG.debug(":::Executing Row: "+a);
            JSONObject jsonObject = jsonArray.getJSONObject(a);
            LOG.debug(":::Full Tree Json Object: "+jsonObject);
            SQLparams = jsonObject.getJSONObject("onPremise");
            solrParams = jsonObject.getJSONObject("solr");
            sqlToSolrColMapping = jsonObject.getJSONObject("mappings");
            SQLColList = SQLparams.getJSONArray("select");

            test = report.startTest(SQLparams.getString("from") + "-" + solrParams.getString("collection"));

            onPremise onpremiseObj = new onPremise();
            JSONArray onPremiseData = onpremiseObj.getData(paramDetails,SQLparams);

            solr solrObj = new solr();
            JSONArray solrData = solrObj.getData(paramDetails,solrParams,sqlToSolrColMapping);

            int onPremiseDataLen = onPremiseData.length();
            int solrDataLen = solrData.length();
            String msg = null;
            if(onPremiseDataLen == solrDataLen){
                for(int i=0;i<onPremiseDataLen;i++){
                    JSONObject tempSQLDataObj = (JSONObject) onPremiseData.get(i);
                    JSONObject tempSOLRDataObj = (JSONObject) solrData.get(i);

                    for (int j=0;j<SQLColList.length();j++) {
                        if (!tempSQLDataObj.getString(SQLColList.get(j).toString()).equals(tempSOLRDataObj.getString(SQLColList.get(j).toString()))) {
                            LOG.debug("Data Mismatch in collection- " + solrParams.get("collection") + " SQL Data: " + tempSQLDataObj.toString() + " SOLR Data: " + tempSOLRDataObj.toString());
                            msg = "<b> Column Name :</b>  " + SQLColList.get(j).toString() + " <br> " +
                                    "<b> SQL Column Data : </b> " + tempSQLDataObj.getString(SQLColList.get(j).toString()) +
                                    "<br> <b> SOLR Column Data : </b>" + tempSOLRDataObj.getString(SQLColList.get(j).toString())+
                                    "<br><br> <b>SQL Data:</b> "+tempSQLDataObj.toString()+" <br/> <b>SOLR Data: </b>"+tempSOLRDataObj.toString();
                            test.log(LogStatus.FAIL, msg);
                        }
                    }
                }
            }else{
                LOG.debug("Rows mismatch, OnpremiseData: "+onPremiseDataLen+" solrData: "+ solrDataLen);
                resultMessages.put("Rows mismatch, OnpremiseData: "+onPremiseDataLen+" solrData: "+ solrDataLen);
                test.log(LogStatus.FAIL, "Rows mismatch,<br/> <b>OnpremiseData:</b> "+onPremiseDataLen+" <br> <b>solrData:</b> "+ solrDataLen);
            }
        }
        LOG.debug("::: Final result messages: "+resultMessages);
        report.endTest(test);
        report.flush();
//        sendEmail.sendmail(fileName,new JSONArray(),"sunil.gudivada@talentica.com");
    }
}
